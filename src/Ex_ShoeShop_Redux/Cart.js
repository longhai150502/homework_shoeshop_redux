import React, { Component } from "react";
import { connect } from "react-redux";
import { INCREMENT_PRODUCT, DECREMENT_PRODUCT, DELETE_PRODUCT } from "./redux/constant/shoeContant";


class Cart extends Component {
  renderTbody = () => {
    return this.props.gioHang.map((item) => {
      return (
        <tr>
          <td>{item.name}</td>
          <td>${item.price * item.number}</td>
          <td>
            <button className="btn btn-dark btn-sm" onClick={() => {this.props.decrementProduct(item)}} >-</button>
            {item.number}
            <button className="btn btn-dark btn-sm" onClick={() => {this.props.incrementProduct(item)}}>+</button>
          </td>
          <td>
            <img style={{ width: "80px" }} src={item.image} alt="" />
          </td>
          <td><button onClick={() => {this.props.deleteProduct(item)}} className="btn btn-danger">X</button></td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeReducer.cart,
  };
};


let mapDispatchToProps = (dispatch) => {
  return {
    incrementProduct: (cartItem) => {
      let action = {
        type: INCREMENT_PRODUCT,
        payload: cartItem,
      };
      dispatch(action);
    },
    decrementProduct: (cartItem) => {
      let action = {
        type: DECREMENT_PRODUCT,
        payload: cartItem,
      };
      dispatch(action);
    },
    deleteProduct: (cartItem) => {
      let action = {
        type: DELETE_PRODUCT,
        payload: cartItem,
      };
      dispatch(action);
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
