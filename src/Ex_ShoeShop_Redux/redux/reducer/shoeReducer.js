/* eslint-disable eqeqeq */
import { dataShoe } from "../../dataShoe";
import { ADD_TO_CART, CHANGE_DETAIL, INCREMENT_PRODUCT, DECREMENT_PRODUCT, DELETE_PRODUCT } from "../constant/shoeContant";

let initailState = {
  shoeArr: dataShoe,
  detail: [],
  cart: [],
};

export const shoeReducer = (state = initailState, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        let cartItem = { ...action.payload, number: 1 };
        cloneCart.push(cartItem);
      } else {
        cloneCart[index].number++;
      }
      return { ...state, cart: cloneCart };
    }
    case CHANGE_DETAIL: {
      state.detail = action.payload;
      return { ...state };
    }
    case INCREMENT_PRODUCT: {
      let cartItem = [...state.cart];
      let index = cartItem.findIndex((item) => {
        return item.id == action.payload.id
      });
      cartItem[index].number++;
      return {...state,cart:cartItem};
    }
    case DECREMENT_PRODUCT: {
      let cartItem = [...state.cart];
      let index = cartItem.findIndex((item) => {
        return item.id == action.payload.id
      });
      if(cartItem[index].number >= 2){
        cartItem[index].number--;
      }
      return {...state, cart:cartItem};
    }
    case DELETE_PRODUCT: {
      let cartItem = [...state.cart];
      let index = cartItem.findIndex((item) => {
        return item.id == action.payload.id
      });
      cartItem.splice(index, 1);
      return {...state ,cart: cartItem};
    }
    default:
      return state;
  }
};

